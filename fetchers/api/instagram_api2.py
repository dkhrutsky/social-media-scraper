import requests
import logging
# import threading
# import time
import random
import datetime
from json import JSONDecodeError


# Based on https://github.com/postaddictme/instagram-php-scraper/blob/master/src/InstagramScraper/Endpoints.php
BASE_URL = 'https://www.instagram.com'
LOGIN_URL = 'https://www.instagram.com/accounts/login/ajax/'
ACCOUNT_PAGE = 'https://www.instagram.com/{username}'
MEDIA_LINK = 'https://www.instagram.com/p/{code}'
ACCOUNT_MEDIAS = 'https://www.instagram.com/{username}/media/?max_id={max_id}'
ACCOUNT_JSON_INFO = 'https://www.instagram.com/{username}/?__a=1'
MEDIA_JSON_INFO = 'https://www.instagram.com/p/{code}/?__a=1'
MEDIA_JSON_BY_LOCATION_ID = 'https://www.instagram.com/explore/locations/{facebookLocationId}/?__a=1&max_id={max_id}'
MEDIA_JSON_BY_TAG = 'https://www.instagram.com/explore/tags/{tag}/?__a=1&max_id={max_id}'
GENERAL_SEARCH = 'https://www.instagram.com/web/search/topsearch/?query={query}'
ACCOUNT_JSON_INFO_BY_ID = 'ig_user({user_id}){id,username,external_url,full_name,profile_pic_url,biography,followed_by{count},follows{count},media{count},is_private,is_verified}'
COMMENTS_BEFORE_COMMENT_ID_BY_CODE = 'https://www.instagram.com/graphql/query/?query_id=17852405266163336&shortcode={shortcode}&first={count}&after={comment_id}'
LAST_LIKES_BY_CODE = 'ig_shortcode({code}){likes{nodes{' \
                     'id,user{id,profile_pic_url,username,follows{count},followed_by{count},biography,full_name,media{count},is_private,external_url,is_verified}},' \
                     'page_info}}'
FOLLOWING_URL = 'https://www.instagram.com/graphql/query/?query_id=17874545323001329&id={account_id}&first={count}'
FOLLOWERS_URL = 'https://www.instagram.com/graphql/query/?query_id=17851374694183129&id={account_id}&first={count}'
FOLLOW_URL = 'https://www.instagram.com/web/friendships/{{accountId}}/follow/'
UNFOLLOW_URL = 'https://www.instagram.com/web/friendships/{{accountId}}/unfollow/'
USER_FEED = 'https://www.instagram.com/graphql/query/?query_id=17861995474116400&fetch_media_item_count=12&fetch_media_item_cursor=&fetch_comment_count=4&fetch_like=10'
USER_FEED2 = 'https://www.instagram.com/?__a=1'
INSTAGRAM_QUERY_URL = 'https://www.instagram.com/query/'
INSTAGRAM_CDN_URL = 'https://scontent.cdninstagram.com/'
ACCOUNT_MEDIAS2 = 'https://www.instagram.com/graphql/query/?query_id=17880160963012870&id={{accountId}}&first=10&after='

URL_SIMILAR = 'https://www.instagram.com/graphql/query/?query_id=17845312237175864&id=4663052'
GRAPH_QL_QUERY_URL = 'https://www.instagram.com/graphql/query/?query_id={query_id}'

USER_MEDIAS = '17880160963012870'
MAX_COMMENTS_PER_REQUEST = 300
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'


class InstagramAPIException(Exception):
    pass


class Account(object):

    def __init__(self, source='account_page', data=None):
        logging.info('Creating account from ' + source)
        self.medias = []
        getattr(self, 'init_{}'.format(source))(data)

    def init_account_page(self, user_data):
        self.username = user_data['username']
        self.follows_count = user_data['follows']['count']
        self.followed_by_count = user_data['followed_by']['count']
        self.id = user_data['id']
        self.biography = user_data['biography']
        self.full_name = user_data['full_name']
        self.media_count = user_data['media']['count']
        self.is_private = user_data.get('is_private')

    def init_media_page(self, user_data):
        self.id = user_data['id']
        self.profile_pic_url = user_data.get('profile_pic_url') or user_data.get('profile_picture')
        self.username = user_data['username']
        self.full_name = user_data['full_name']
        self.is_private = user_data.get('is_private')

    def init_comment(self, user_data):
        self.id = user_data['id']
        self.profile_pic_url = user_data.get('profile_pic_url')
        self.username = user_data['username']

    def set_medias(self, medias):
        self.medias = medias


class Media(object):

    def __init__(self, source='media_page', data=None):
        logging.info('Creating media from ' + source)
        getattr(self, 'init_{}'.format(source))(data)

    def get_image_urls(self, url):
        image_name = url.split('/')[-1]
        urls = {
            'thumbnail': INSTAGRAM_CDN_URL + 't/s150x150/' + image_name,
            'low': INSTAGRAM_CDN_URL + 't/s320x320/' + image_name,
            'standard': INSTAGRAM_CDN_URL + 't/s640x640/' + image_name,
            'high': INSTAGRAM_CDN_URL + 't/' + image_name,
        }
        return urls

    def init_media_page(self, media_data):
        self.id = media_data['id']
        self.type = 'image'
        if media_data['is_video']:
            self.type = 'video'
            self.video_url = media_data['video_url']
            self.video_views = media_data['video_view_count']
        if media_data.get('caption_is_edited'):
            self.caption_is_edited = media_data['caption_is_edited']
        if media_data.get('is_ad'):
            self.is_ad = media_data['is_ad']
        self.created_time = media_data['taken_at_timestamp']
        self.shortcode = media_data['shortcode']
        self.link = MEDIA_LINK.format(code=self.shortcode)
        self.comments_count = media_data['edge_media_to_comment']['count']
        self.likes_count = media_data['edge_media_preview_like']['count']
        images = self.get_image_urls(media_data['display_url'])
        self.image_thumbnail_url = images['thumbnail']
        self.image_low_resolution_url = images['low']
        self.image_standard_resolution_url = images['standard']
        self.image_high_resolution_url = images['high']
        try:
            self.caption = media_data['edge_media_to_caption']['edges'][0]['node']['text']
        except (KeyError, IndexError):
            pass
        if media_data.get('location'):
            self.location_id = media_data['location'].get('id')
            self.location_name = media_data['location'].get('name')
        self.owner = Account('media_page', media_data['owner'])
        self.owner_id = self.owner.id

    def init_api(self, media_data):
        self.id = media_data['id']
        self.type = media_data['type']
        self.created_time = media_data['created_time']
        self.shortcode = media_data['code']
        self.link = media_data['link']
        self.comments_count = media_data['comments']['count']
        self.likes_count = media_data['likes']['count']
        images = self.get_image_urls(media_data['images']['standard_resolution']['url'])
        self.image_thumbnail_url = images['thumbnail']
        self.image_low_resolution_url = images['low']
        self.image_standardResolutionUrl = images['standard']
        self.image_high_resolution_url = images['high']
        try:
            self.caption = media_data['caption']['text']
        except (KeyError, TypeError):
            self.caption = ''
        if self.type == 'video':
            self.video_views = media_data.get('video_views')
            if media_data.get('videos'):
                self.video_low_resolution_url = media_data['videos']['low_resolution']['url']
                self.video_standard_resolution_url = media_data['videos']['standard_resolution']['url']
                self.video_low_bandwidth_url = media_data['videos']['low_bandwidth']['url']
        if media_data.get('location'):
            self.location_id = media_data['location'].get('id')
            self.location_name = media_data['location'].get('name')
        self.owner = Account('media_page', media_data['user'])
        self.owner_id = media_data['user']['id']

    def set_comments(self, comments):
        self.comments = comments


class Comment(object):

    def __init__(self, data):
        logging.info('Creating comment')
        self.id = data['id']
        self.created_at = data['created_at']
        self.text = data['text']
        self.owner = Account('comment', data['owner'])


class Instagram(object):

    def __init__(self, *args, **kwargs):
        self.session = {}

    # Get account
    def get_account_by_username(self, username):
        response = requests.get(ACCOUNT_JSON_INFO.format(username=username))
        if response.status_code != 200:
            err_msg = 'HTTP {0}: {1} (username: {2})'.format(response.status_code, response.content, username)
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)
        else:
            logging.info('Got account {}: {}'.format(username, response.content[:100]))
            user_data = response.json()['user']
            account = Account('account_page', user_data)
            return account

    def get_account_by_id(self, id):
        try:
            id = int(id)
        except ValueError:
            raise InstagramAPIException('Wrong account ID. Must be numeric')
        response = requests.get(GRAPH_QL_QUERY_URL.format(query_id=USER_MEDIAS), params={'id': id, 'first': 1})
        if response.status_code != 200:
            err_msg = 'HTTP {0}: {1} (user ID: {2})'.format(response.status_code, response.content, id)
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)

        logging.info('Got account ID#{}: {}'.format(id, response.content[:100]))
        self.session['csrftoken'] = response.cookies['csrftoken']
        user_data = response.json()
        if user_data['status'] == 'fail':
            err_msg = 'HTTP {0}: {1} (user ID: {2})'.format(response.status_code, user_data['message'], id)
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)
        if not user_data.get('data', {}).get('user'):
            err_msg = 'User with ID {} not found'.format(id)
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)
        edges = user_data['data']['user']['edge_owner_to_timeline_media']['edges']
        if len(edges) == 0:
            err_msg = 'User ID#{} exists but information is unavailable'.format(id)
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)
        shortcode = edges[0]['node']['shortcode']
        media = self.get_media_by_url(MEDIA_LINK.format(code=shortcode))
        return media.owner

    # Get account posts
    def get_account_medias(self, username, count=20, max_id=None):
        index = 0
        medias = []
        is_more_available = True
        while index < count and is_more_available:
            response = requests.get(ACCOUNT_MEDIAS.format(username=username, max_id=str(max_id)))
            if response.status_code != 200:
                err_msg = 'HTTP {0}: {1} (username: {2})'.format(response.status_code, response.content[:100], username)
                logging.error(err_msg)
                raise InstagramAPIException(err_msg)
            try:
                medias_data = response.json()
            except JSONDecodeError:
                err_msg = 'HTTP {0}: {1} (username: {2})'.format(response.status_code, response.content[:100], username)
                logging.error(err_msg)
                raise InstagramAPIException(err_msg)
            if not len(medias_data['items']):
                return medias
            for media in medias_data['items']:
                if index == count:
                    return medias
                medias.append(Media('api', media))
                index += 1
            max_id = medias_data['items'][-1]['id']
            is_more_available = medias_data['more_available']

        return medias

    def generate_headers(self):
        headers = {
            'cookie': '; '.join(['{}={}'.format(key, str(value)) for key, value in self.session.items()]) + '; ',
            'referer': BASE_URL + '/',
            'x-csrftoken': self.session['csrftoken']
        }
        return headers

    # Get post
    def get_media_by_url(self, url):
        # TODO: Filter URL?
        response = requests.get(url, params={'__a': 1}, headers=self.generate_headers())
        if response.status_code != 200:
            err_msg = 'HTTP {0}: {1} (url: {2})'.format(response.status_code, response.content, url)
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)

        logging.info('Got media: {}'.format(response.content[:100]))
        media_data = response.json()
        if not media_data['graphql'].get('shortcode_media'):
            err_msg = 'Media with this code does not exist'
            logging.error(err_msg)
            raise InstagramAPIException(err_msg)
        media = Media('media_page', media_data['graphql']['shortcode_media'])
        return media

    def get_media_comments_by_id(self, media_id, count=10, max_id=None):
        code = get_media_code_from_id(media_id)
        return self.get_media_comments_by_code(code)

    def get_media_comments_by_code(self, code, count=10, max_id=None):
        remained = count
        comments = []
        index = 0
        has_previous = True
        while has_previous and index < count:
            if remained > MAX_COMMENTS_PER_REQUEST:
                num_comments_to_get = self.MAX_COMMENTS_PER_REQUEST
                remained -= num_comments_to_get
                index += num_comments_to_get
            else:
                num_comments_to_get = remained
                index += remained
                remained = 0
            max_id = max_id or ''
            comments_url = self.get_comments_before_comment_url(code, num_comments_to_get, max_id)
            response = requests.get(comments_url, headers=self.generate_headers())
            if response.status_code != 200:
                err_msg = 'HTTP {0}: {1} (comments)'.format(response.status_code, response.content, id)
                logging.error(err_msg)
                raise InstagramAPIException(err_msg)
            self.session['csrftoken'] = response.cookies['csrftoken']
            comments_data = response.json()
            nodes = comments_data['data']['shortcode_media']['edge_media_to_comment']['edges']
            for node in nodes:
                comments.append(Comment(node['node']))
            has_previous = comments_data['data']['shortcode_media']['edge_media_to_comment']['page_info']['has_next_page']
            number_of_comments = comments_data['data']['shortcode_media']['edge_media_to_comment']['count']
            if count > number_of_comments:
                count = number_of_comments
            if len(nodes) != 0:
                max_id = nodes[-1]['node']['id']
            return comments

    def get_account_followers(self, username):
        pass

    def get_comments_before_comment_url(self, code, count, comment_id):
        url = COMMENTS_BEFORE_COMMENT_ID_BY_CODE.format(shortcode=code, count=count, comment_id=comment_id)
        return url


def get_media_code_from_id(id):
    parts = id.split('_')
    id = int(parts[0])
    code = ''
    while id > 0:
        remainder = id % 64
        id = (id - remainder) // 64
        code = ALPHABET[remainder] + code
    return code


def get_media_id_from_code(code):
    id = 0
    for l in code:
        id = id * 64 + ALPHABET.index(l)
    return id


def test_limits(users_count=1, media_count=1, comments_count=1):
    start_time = datetime.datetime.now()
    ig = Instagram()
    # Get U users
    users = []
    for _ in range(users_count):
        try:
            user = ig.get_account_by_id(random.randint(3, 1000000))
            users.append(ig.get_account_by_username(user.username))
        except InstagramAPIException:
            pass
    # Get P posts for each user
    for user in users:
        try:
            medias = ig.get_account_medias(user.username, count=media_count)
            # Get C comments for each post
            for media in medias:
                comments = ig.get_media_comments_by_code(media.shortcode, count=comments_count)
                media.set_comments(comments)
            user.set_medias(medias)
            logging.warning('User ({}: ID#{}). Medias count: {}. Comments count for last media: {}'.format(
                getattr(user, 'username', 'None'),
                getattr(user, 'id', ''),
                getattr(user, 'media_count', 'None'),
                user.medias[0].comments_count if len(user.medias) else 'No media'))
        except InstagramAPIException:
            pass
    elapsed = (datetime.datetime.now() - start_time).total_seconds()
    logging.info('Test time: {}s'.format(elapsed))


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    # logging.basicConfig(level=logging.INFO, filename='instagram.log')
    test_limits(100, 10, 10)
    login, password = ('dkhrutsky', 'JExyomfohuBupp8')
    # threads = []
    # for _ in range(10):
    #     t = threading.Thread(target=test_limits, args=(1000, 20, 20))
    #     threads.append(t)
    #     t.start()
    #     time.sleep(1)

    # for t in threads:
    #     t.join()
