from vk_alchemy import Category, AdvertCampaign, session
import logging
from psycopg2 import IntegrityError


def create_categories():
    default_categories = [
        Category(id=1, name="авто/мото"),
        Category(id=2, name="активный отдых"),
        Category(id=3, name="бизнес"),
        Category(id=4, name="домашние животные"),
        Category(id=5, name="здоровье"),
        Category(id=6, name="знакомство и общение"),
        Category(id=7, name="игры"),
        Category(id=8, name="ИТ (компьютеры и софт),"),
        Category(id=9, name="кино"),
        Category(id=10, name="красота и мода"),
        Category(id=11, name="кулинария"),
        Category(id=12, name="культура и искусство"),
        Category(id=13, name="литература"),
        Category(id=14, name="мобильная связь и интернет"),
        Category(id=15, name="музыка"),
        Category(id=16, name="наука и техника"),
        Category(id=17, name="недвижимость"),
        Category(id=18, name="новости и СМИ"),
        Category(id=19, name="безопасность"),
        Category(id=20, name="образование"),
        Category(id=21, name="обустройство и ремонт"),
        Category(id=22, name="политика"),
        Category(id=23, name="продукты питания"),
        Category(id=24, name="промышленность"),
        Category(id=25, name="путешествия"),
        Category(id=26, name="работа"),
        Category(id=27, name="развлечения"),
        Category(id=28, name="религия"),
        Category(id=29, name="дом и семья"),
        Category(id=30, name="спорт"),
        Category(id=31, name="страхование"),
        Category(id=32, name="телевидение"),
        Category(id=33, name="товары и услуги"),
        Category(id=34, name="увлечения и хобби"),
        Category(id=35, name="финансы"),
        Category(id=36, name="фото"),
        Category(id=37, name="эзотерика"),
        Category(id=38, name="электроника и бытовая техника"),
        Category(id=39, name="эротика"),
        Category(id=40, name="юмор"),
        Category(id=41, name="общество, гуманитарные науки"),
        Category(id=42, name="дизайн и графика")
    ]

    session.add_all(default_categories)
    session.commit()


def create_ads():
    # age_min, age_max, male_min, female_min, cities
    # sub_count, reach, activity, category_id, keywords
    # 1. Female 60%, 20-25, Москва, "дом и семья" (29)
    ads_1 = AdvertCampaign(id=1, age_min=20, age_max=25, female_min=0.6, cities='Москва',
                           sub_count=1000, category_id=29, description='Товары для дома')
    # 2. Male 70%, 30-35, Санкт-Петербург, "ИТ" (8)
    ads_2 = AdvertCampaign(id=2, age_min=30, age_max=35, male_min=0.7, cities="Санкт-Петербург",
                           sub_count=10000, category_id=8, description='НОВЫЕ КОМПЫ')
    # 3. Female 40%, 20-30, Москва, "фото" (36)
    ads_3 = AdvertCampaign(id=3, age_min=20, age_max=30, female_min=0.4,
                           cities="Москва", category_id=36, description='Фоточки')
    # 4. For mediazzzona
    ads_4 = AdvertCampaign(id=4, age_min=21, age_max=24, female_min=0.35, cities="Москва",
                           category_id=18, sub_count=10000, description='Навальный 20!8')
    session.add_all([ads_1, ads_2, ads_3, ads_4])
    session.commit()


if __name__ == '__main__':
    try:
        create_categories()
    except IntegrityError:
        logging.warning('Categories already exist')
    try:
        create_ads()
    except IntegrityError:
        logging.warning('Test ads already exist')
