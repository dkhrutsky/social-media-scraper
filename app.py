from flask import Flask, request, render_template, redirect, flash, url_for, abort, jsonify
from fetchers import ig_alchemy as ig_fetcher
from fetchers import vk_alchemy as vk_fetcher
from multiprocessing import Process
from uuid import uuid4
from sqlalchemy_searchable import search
# import threading
import datetime
import os
# import json

app = Flask(__name__)
app.secret_key = 'mw0paripsadgn7pw450934q4=-f,cqj9'
PROCESSES = []
MAX_PROCESSES_COUNT = 16


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/instagram/')
def show_instagram_data():
    accounts = ig_fetcher.session.query(ig_fetcher.Account).filter(
        ig_fetcher.Account.followed_by_count > 0).order_by(ig_fetcher.Account.followed_by_count.desc()).all()
    return render_template('instagram/data.html',
                           accounts=accounts,
                           # posts=medias,
                           # comments=comments,
                           type='instagram')


@app.route('/instagram/accounts/<string:account_name>')
def show_instagram_account(account_name):
    account = ig_fetcher.session.query(ig_fetcher.Account).filter_by(username=account_name).first() or \
        ig_fetcher.session.query(ig_fetcher.Account).filter_by(id=int(account_name)).first()
    if not account:
        abort(404)
    medias = account.medias
    return render_template('instagram/account.html',
                           account=account,
                           medias=medias)


@app.route('/instagram/medias/<string:media_id>')
def get_instagram_media_history(media_id):
    media = ig_fetcher.session.query(ig_fetcher.Media).get(media_id)
    if not media:
        abort(404)
    history = media.history
    stats = media.count_stats()
    return render_template('instagram/media_history.html',
                           media=media,
                           stats=stats,
                           history=history)


@app.route('/instagram/popular/', defaults={'date': '2017-08-04'})
@app.route('/instagram/popular/<string:date>')
def get_instagram_popular_posts(date):
    date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    medias = ig_fetcher.get_most_popular_medias_for_day(date)
    unique = []
    m_ids = []
    u_ids = []
    for media in medias:
        if media.id not in m_ids and media.owner_id not in u_ids:
            unique.append(media)
            m_ids.append(media.id)
            u_ids.append(media.owner_id)
    return render_template('/instagram/popular.html',
                           medias=unique,
                           date=date)


# @app.route('/instagram/commenters/')
# def get_instagram_commenters():
#     commenters = ig_fetcher.get_active_commenters()
#     return render_template('/instagram/commenters.html',
#                            commenters=commenters)


@app.route('/instagram/commenters/', defaults={'account_name': 'instagram'})
@app.route('/instagram/commenters/<string:account_name>')
def get_instagram_commenters(account_name):
    account = ig_fetcher.session.query(ig_fetcher.Account).filter_by(username=account_name).first()
    if not account:
        abort(404)
    # import ipdb; ipdb.set_trace()
    last_medias_count = 30
    commenters = ig_fetcher.get_active_commenters(account, last_medias_count)
    return render_template('/instagram/commenters.html',
                           commenters=commenters,
                           last_medias_count=last_medias_count,
                           account=account)


@app.route('/instagram/search')
def instagram_search():
    search_query = request.args.get('q', '')
    if search_query:
        search_string = ' or '.join(search_query.split(','))

        accounts = search(ig_fetcher.session.query(ig_fetcher.Account),
                          search_string, sort=True)
        medias = search(ig_fetcher.session.query(ig_fetcher.Media),
                        search_string, sort=True)
        comments = search(ig_fetcher.session.query(ig_fetcher.Comment),
                          search_string, sort=True)
    else:
        accounts, medias, comments = [], [], []
    return render_template('/instagram/search.html',
                           accounts=accounts,
                           medias=medias,
                           comments=comments,
                           search_query=search_query)


@app.route('/instagram/accounts/<string:account_name>/search')
def instagram_search_in_account(account_name):
    search_query = request.args.get('q', '')
    account = ig_fetcher.session.query(ig_fetcher.Account).filter_by(username=account_name).first()
    if not account:
        abort(404)
    if search_query:
        search_string = ' or '.join(search_query.split(','))
        medias = ig_fetcher.session.query(ig_fetcher.Media).filter_by(owner_id=account.id)
        media_ids = [x[0] for x in medias.values(ig_fetcher.Media.id)]
        comments = ig_fetcher.session.query(ig_fetcher.Comment).filter(ig_fetcher.Comment.media_id.in_(media_ids))

        medias = search(medias, search_string, sort=True)
        comments = search(comments, search_string, sort=True)
    else:
        medias, comments = [], []
    return render_template('/instagram/search.html',
                           account=account,
                           medias=medias,
                           comments=comments,
                           search_query=search_query)


@app.route('/vk/')
def show_vk_data():
    groups = vk_fetcher.session.query(vk_fetcher.Group).order_by(vk_fetcher.Group.id.asc())
    posts = []
    comments = []
    # posts = list(vk_fetcher.Post.select().order_by(vk_fetcher.Post.post_id.asc()).limit(100).dicts())
    # comments = list(vk_fetcher.Comment.select().order_by(vk_fetcher.Comment.comment_id.asc()).limit(100).dicts())
    return render_template('vk/data.html',
                           accounts=groups,
                           posts=posts,
                           comments=comments,
                           type='vk')


@app.route('/vk/groups/<int:group_id>')
def show_vk_group(group_id):
    group = vk_fetcher.session.query(vk_fetcher.Group).get(group_id)
    if not group:
        abort(404)
    posts = group.posts[:100]
    stats = vk_fetcher.get_audience_characteristics(group)
    ads = vk_fetcher.find_ads_for_group(group, stats) if stats else []
    return render_template('vk/group.html',
                           group=group,
                           stats=stats,
                           ads=ads,
                           posts=posts)


@app.route('/vk/commenters/', defaults={'group_id': 76982440})
@app.route('/vk/commenters/<int:group_id>')
def get_vk_group_commenters(group_id):
    group = vk_fetcher.session.query(vk_fetcher.Group).get(group_id)
    if not group:
        abort(404)
    last_posts_count = 10
    commenters = vk_fetcher.get_active_commenters(group, last_posts_count)
    return render_template('/vk/commenters.html',
                           commenters=commenters,
                           last_posts_count=last_posts_count,
                           group=group)


@app.route('/vk/comments/', defaults={'group_id': 76982440})
@app.route('/vk/comments/<int:group_id>')
def get_vk_group_top_comments(group_id):
    group = vk_fetcher.session.query(vk_fetcher.Group).get(group_id)
    if not group:
        abort(404)
    last_posts_count = 20
    comments = vk_fetcher.get_popular_comments(group, last_posts_count)
    return render_template('/vk/comments.html',
                           comments=comments,
                           last_posts_count=last_posts_count,
                           group=group)


@app.route('/vk/posts/<string:post_pk>')
def get_vk_post_history(post_pk):
    post = vk_fetcher.session.query(vk_fetcher.Post).get(post_pk)
    if not post:
        abort(404)
    history = post.history
    stats = post.count_stats()
    return render_template('vk/post_history.html',
                           post=post,
                           stats=stats,
                           history=history)


@app.route('/vk/popular/', defaults={'date': '2017-07-19'})
@app.route('/vk/popular/<string:date>')
def get_vk_popular_posts(date):
    date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
    posts = vk_fetcher.get_most_popular_posts_for_day(date)
    unique = []
    ids = []
    g_ids = []
    for post in posts:
        if post.pk not in ids and post.owner_id not in g_ids:
            unique.append(post)
            ids.append(post.pk)
            g_ids.append(post.owner_id)
    return render_template('/vk/popular.html',
                           posts=unique,
                           date=date)


@app.route('/vk/search')
def vk_search():
    search_query = request.args.get('q', '')
    if search_query:
        search_string = ' or '.join(search_query.split(','))
        posts = search(vk_fetcher.session.query(vk_fetcher.Post),
                       search_string, sort=True)
        comments = search(vk_fetcher.session.query(vk_fetcher.Comment),
                          search_string, sort=True)
    else:
        posts, comments = [], []
    return render_template('/vk/search.html',
                           posts=posts,
                           comments=comments,
                           search_query=search_query)


@app.route('/vk/groups/<int:group_id>/search')
def vk_search_in_group(group_id):
    search_query = request.args.get('q', '')
    group = vk_fetcher.session.query(vk_fetcher.Group).get(group_id)
    if not group:
        abort(404)
    if search_query:
        search_string = ' or '.join(search_query.split(','))
        posts = vk_fetcher.session.query(vk_fetcher.Post).filter_by(group_id=group_id)
        comments = vk_fetcher.session.query(vk_fetcher.Comment).filter(vk_fetcher.Comment.post_id.like(f'-{group_id}%'))
        posts = search(posts, search_string, sort=True)
        comments = search(comments, search_string, sort=True)
    else:
        posts, comments = [], []
    return render_template('/vk/search.html',
                           group=group,
                           posts=posts,
                           comments=comments,
                           search_query=search_query)


@app.route('/api/vk/posts/<string:post_pk>')
def get_vk_post_history_json(post_pk):
    post = vk_fetcher.session.query(vk_fetcher.Post).get(post_pk)
    if not post:
        abort(404)
    history = []
    for record in post.history:
        del record.__dict__['_sa_instance_state']
        history.append(record.__dict__)
    return jsonify(history)


@app.route('/api/vk/groups/<int:group_id>')
def get_vk_group_audience_json(group_id):
    group = vk_fetcher.session.query(vk_fetcher.Group).get(group_id)
    if not group:
        abort(404)
    stats = vk_fetcher.get_audience_characteristics(group)
    return jsonify(stats)


@app.route('/api/instagram/posts/<string:media_id>')
def get_instagram_media_history_json(media_id):
    media = ig_fetcher.session.query(ig_fetcher.Media).get(media_id)
    if not media:
        abort(404)
    history = []
    for record in media.history:
        del record.__dict__['_sa_instance_state']
        history.append(record.__dict__)
    return jsonify(history)


@app.route('/ads')
def get_advert_campaigns():
    ads = vk_fetcher.session.query(vk_fetcher.AdvertCampaign).all()
    return render_template('/vk/ads.html', ads=ads)


# @app.route('/vk/import', methods=['GET', 'POST'])
# def import_vk_json():
#     if request.method == 'GET':
#         return render_template('/vk/import.html')
#     if request.method == 'POST':
#         if 'vk_data' not in request.files:
#             flash('No data', 'error')
#             return redirect(request.url)
#         json_file = request.files['vk_data']
#         if json_file.filename == '':
#             flash('No data', 'error')
#             return redirect(request.url)
#         if json_file and '.' in json_file.filename and json_file.filename.rsplit('.', 1)[1].lower() == 'json':
#             vk_data_json = json_file.read()
#             try:
#                 vk_data = json.loads(vk_data_json)
#             except json.JSONDecodeError:
#                 flash('Data is not JSON', 'error')
#                 return redirect(request.url)
#             groups = vk_data.get('groups', [])
#             for group_data in groups:
#                 pass
#             posts = vk_data.get('posts', [])
#             for post_data in posts:
#                 pass
#             history = vk_data.get('history', [])
#             for record_data in history:
#                 pass
#             flash('VK data uploaded', 'success')
#             return redirect(request.url)


@app.route('/vk/start', methods=['POST'])
def start_vk_api():
    groups = request.form.get('vk_groups')
    groups = [x.strip() for x in groups.split(',')]
    if not groups:
        flash('Script launch failed: no accounts provided.', 'error')
        return redirect(url_for('show_running_jobs'))
    alive_processes_count = len([p['process'].is_alive() for p in PROCESSES])
    if alive_processes_count < MAX_PROCESSES_COUNT:
        flash(f'Started fetching VK data for {str(groups)}', 'success')
        p = Process(target=vk_fetcher.collect_data, args=(groups,))
        p.start()
        PROCESSES.append({
            'type': 'VK',
            'args': groups,
            'started': datetime.datetime.now(),
            'process': p,
            'uuid': uuid4(),
        })
    else:
        flash('Script launch failed: too many jobs. Try again later.', 'warning')
    return redirect(url_for('show_running_jobs'))


@app.route('/instagram/start', methods=['POST'])
def start_instagram_api():
    accounts = request.form.get('ig_accounts')
    accounts = [x.strip() for x in accounts.split(',')]
    if not accounts:
        flash('Script launch failed: no accounts provided.', 'error')
        return redirect(url_for('show_running_jobs'))
    p = Process(target=ig_fetcher.collect_data, args=(accounts,))
    alive_threads_count = len([p['process'].is_alive() for p in PROCESSES])
    if alive_threads_count < MAX_PROCESSES_COUNT:
        flash(f'Started fetching Instagram data for {str(accounts)}', 'success')
        p.start()
        PROCESSES.append({
            'type': 'VK',
            'args': accounts,
            'started': datetime.datetime.now(),
            'process': p,
            'uuid': uuid4(),
        })
    else:
        flash('Script launch failed: too many jobs. Try again later.', 'warning')
    return redirect(url_for('show_running_jobs'))


@app.route('/jobs/')
def show_running_jobs():
    jobs = []
    for p_dict in PROCESSES:
        d = p_dict.copy()
        d['running'] = d['process'].is_alive()
        jobs.append(d)
    return render_template('jobs.html', jobs=jobs)


@app.route('/jobs/delete/<uuid:thread_uuid>')
def stop_job(thread_uuid):
    try:
        p_dict = next((item for item in PROCESSES if item["uuid"] == thread_uuid))
        p_dict['process'].terminate()
        flash(f'Stopped job {thread_uuid}', 'success')
    except StopIteration:
        flash('No job found with given UUID', 'warning')
    except Exception as e:
        print(str(e))
        flash(f'Unexpected error: {str(e)}', 'error')
    return redirect(url_for('show_running_jobs'))


if os.environ.get('HEROKU') is not None:
    import logging
    stream_handler = logging.StreamHandler()
    app.logger.addHandler(stream_handler)
    app.logger.setLevel(logging.INFO)
    app.logger.info('Starting...')
