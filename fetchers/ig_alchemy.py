import logging
import datetime
import os
import json
from .api.instagram_api import InstagramAPI

from sqlalchemy import Column, ForeignKey, Integer, BigInteger, SmallInteger, String, Text, DateTime, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, configure_mappers
from sqlalchemy import create_engine, func
from sqlalchemy_searchable import make_searchable
from sqlalchemy_utils.types import TSVectorType


INSTAGRAM_CDN_URL = 'https://scontent.cdninstagram.com/'
MEDIA_LINK = 'https://www.instagram.com/p/{}'
DB_URI = os.environ.get('DB_URI', 'postgres://smminer:smminer@127.0.0.1:5432/smminer')
LOGIN = os.environ.get('IG_LOGIN', 'dkhrutsky')
PASSWORD = os.environ.get('IG_PASSWORD', '2m2vJdA3#F8C!ct')

Base = declarative_base()
make_searchable(options={'regconfig': 'pg_catalog.russian'})


def get_or_create(session, model, **kwargs):
    # Need to commit manually
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        session.add(instance)
        return instance, True


class InstagramAPIException(Exception):
    pass


class Account(Base):
    __tablename__ = 'ig_accounts'
    id = Column(BigInteger, primary_key=True)
    username = Column(String(100), nullable=False)

    full_name = Column(String(250))
    is_private = Column(Boolean)

    follows_count = Column(Integer)
    followed_by_count = Column(Integer)
    biography = Column(Text)
    biography_vector = Column(TSVectorType('biography', regconfig='pg_catalog.russian'))
    media_count = Column(Integer)

    profile_pic_url = Column(String(1000))

    category = Column(String(100))
    medias = relationship('Media', back_populates='owner', order_by='Media.created_time')
    comments = relationship('Comment', back_populates='owner')

    def __str__(self):
        return f'<Account {self.id}: {self.username}>'


class Media(Base):
    __tablename__ = 'ig_medias'
    id = Column(String(100), primary_key=True)
    # 1 - photo; 2 - video
    type = Column(SmallInteger, nullable=False)

    video_url = Column(String(1000))
    video_views = Column(Integer)

    caption_is_edited = Column(Boolean, default=False)

    is_ad = Column(Boolean, default=False)

    created_time = Column(DateTime)
    shortcode = Column(String(50))
    link = Column(String(1000))
    comment_count = Column(Integer)
    like_count = Column(Integer)

    image_thumbnail_url = Column(String(1000))
    image_low_resolution_url = Column(String(1000))
    image_standard_resolution_url = Column(String(1000))
    image_high_resolution_url = Column(String(1000))

    caption = Column(Text)
    caption_vector = Column(TSVectorType('caption', regconfig='pg_catalog.russian'))

    location_id = Column(BigInteger)
    location_name = Column(String(1000))

    owner_id = Column(BigInteger, ForeignKey('ig_accounts.id'))
    owner = relationship('Account', back_populates='medias')

    video_low_resolution_url = Column(String(1000))
    video_standard_resolution_url = Column(String(1000))
    video_low_bandwidth_url = Column(String(1000))

    comments = relationship('Comment', back_populates='media')
    history = relationship('MediaHistoryRecord', back_populates='media', order_by='MediaHistoryRecord.timestamp')

    def get_media_pk(self):
        return self.id.split('_')[0]

    def count_stats(self):
        q = self.history
        if len(q) < 2:
            return {}
        first_record, last_record = q[0], q[-1]

        likes_diff = (last_record.likes_count or 0) - (first_record.likes_count or 0)
        comments_diff = (last_record.comments_count or 0) - (first_record.comments_count or 0)
        time_diff = (last_record.timestamp - first_record.timestamp).total_seconds()

        likes_speed = likes_diff / time_diff
        comments_speed = comments_diff / time_diff
        return {'period': time_diff, 'begin': first_record.timestamp, 'end': last_record.timestamp,
                'likes_diff': likes_diff, 'likes_speed': likes_speed,
                'comments_diff': comments_diff, 'comments_speed': comments_speed,
                }

    def __str__(self):
        return f'<Media {self.id}: {self.shortcode}>'


class Comment(Base):
    __tablename__ = 'ig_comments'
    id = Column(BigInteger, primary_key=True)
    created_at = Column(DateTime)
    text = Column(Text)
    text_vector = Column(TSVectorType('text', regconfig='pg_catalog.russian'))
    owner_id = Column(BigInteger, ForeignKey('ig_accounts.id'))
    owner = relationship('Account', back_populates='comments')
    media_id = Column(String(100), ForeignKey('ig_medias.id'))
    media = relationship('Media', back_populates='comments')

    def __str__(self):
        return f'<Comment {self.id}>'


class MediaHistoryRecord(Base):
    __tablename__ = 'ig_media_history_records'
    id = Column(BigInteger, primary_key=True)
    media_id = Column(String(100), ForeignKey('ig_medias.id'))
    media = relationship('Media', back_populates='history')
    timestamp = Column(DateTime, default=datetime.datetime.now)
    likes_count = Column(Integer)
    comments_count = Column(Integer)


# DB_URI = 'postgres://smminer:smminer@127.0.0.1:5432/smminer'

engine = create_engine(DB_URI)
configure_mappers()
Base.metadata.create_all(engine)

DBSession = sessionmaker(bind=engine)
session = DBSession()


def get_image_urls(candidates):
    urls = {}
    name_from_size = {150: 'thumbnail', 320: 'low', 640: 'standard', 750: 'high', 1080: 'high'}
    for c in candidates:
        try:
            urls[name_from_size[c['width']]] = c['url']
        except KeyError:
            pass
    return urls


def collect_data(accounts_list):
    ig = InstagramAPI(LOGIN, PASSWORD)
    result = ig.login()
    if not result:
        raise InstagramAPIException('Cannot login to Instagram. Check credentials.')
    accounts = get_accounts(ig, accounts_list)
    for account in accounts:
        medias = get_account_feed(ig, account)
        for media in medias:
            get_media_comments(ig, media)


def get_accounts(ig, accounts_list):
    accounts = []
    for username in accounts_list:
        ig.searchUsername(username)
        account_data = ig.LastJson
        if not ig.LastResponse.ok:
            logging.error(f'Request failed HTTP {ig.LastResponse.status_code}: {ig.LastResponse.text}')
            continue
        account_data = account_data['user']
        account, created = get_or_create(
            session, Account,
            id=account_data['pk'],
            username=account_data['username'])
        account.full_name = account_data['full_name']
        account.is_private = account_data['is_private']
        account.follows_count = account_data.get('following_count')
        account.followed_by_count = account_data.get('follower_count')
        account.biography = account_data.get('biography')
        account.media_count = account_data.get('media_count')
        account.profile_pic_url = account_data.get('profile_pic_url')
        account.category = account_data.get('category')
        session.add(account)
        session.commit()  # TODO: Move from loop
        logging.info('Got IG account {}'.format(account.username))
        accounts.append(account)
    return accounts


def get_account_feed(ig, account):
    ig.getUserFeed(account.id)
    medias_list = ig.LastJson.get('items', [])
    medias = []
    if not ig.LastResponse.ok:
        logging.error(f'Request failed HTTP {ig.LastResponse.status_code}: {ig.LastResponse.text}')
        return []
    for media_data in medias_list:
        media = session.query(Media).filter_by(id=media_data['id']).first()
        if not media:
            media = Media(
                id=media_data['id'],
                owner=account,
                type=media_data['media_type'],
                shortcode=media_data['code'],
                created_time=datetime.datetime.fromtimestamp(media_data.get('taken_at')),
                link=MEDIA_LINK.format(media_data['code']))
        if media.type == 1:  # Image
            images = get_image_urls(media_data['image_versions2']['candidates'])
            media.image_high_resolution_url = images.get('high')
            media.image_standard_resolution_url = images.get('standard')
            media.image_low_resolution_url = images.get('low')
            media.image_thumbnail_url = images.get('thumbnail')
        media.comment_count = media_data.get('comment_count')
        media.like_count = media_data.get('like_count')
        media.caption_is_edited = media_data.get('caption_is_edited')
        media.is_ad = media_data.get('is_ad')
        media.caption = (media_data.get('caption', {}) or {}).get('text')
        media.location_id = media_data.get('location', {}).get('pk')
        media.location_name = media_data.get('location', {}).get('name')
        session.add(media)
        medias.append(media)
        mhr = MediaHistoryRecord(media=media,
                                 likes_count=media.like_count,
                                 comments_count=media.comment_count)
        session.add(mhr)
        session.commit()  # TODO: Move from loop
        logging.info('Got IG media {} of {}'.format(media.id, media.owner.username))
    return medias


def get_media_comments(ig, media):
    ig.getMediaComments(media.id)
    if not ig.LastResponse.ok:
        logging.error(f'Request failed HTTP {ig.LastResponse.status_code}: {ig.LastResponse.text}')
        return []
    comments = []
    comments_list = ig.LastJson.get('comments', [])
    for comment_data in comments_list:
        owner = session.query(Account).filter_by(id=comment_data['user']['pk']).first()
        if not owner:
            owner = Account(
                id=comment_data['user']['pk'],
                username=comment_data['user']['username'])
            session.add(owner)
        comment, created = get_or_create(
            session, Comment,
            owner=owner,
            media=media,
            id=comment_data['pk'],
            created_at=datetime.datetime.fromtimestamp(comment_data['created_at']),
            text=comment_data['text'],
        )
        session.add(comment)
        session.commit()  # TODO: Move from loop
        comments.append(comment)
        logging.info('Got {} to {} from {}'.format(comment, comment.media, comment.owner))
    return comments


def get_most_popular_medias_for_day(date, count=100):
    # TODO: Refactor for SQLAlchemy
    records = session.query(Media).join(MediaHistoryRecord, Media.id == MediaHistoryRecord.media_id).filter(
        (MediaHistoryRecord.likes_count > 0) &
        (MediaHistoryRecord.timestamp >= date) &
        (MediaHistoryRecord.timestamp < (date + datetime.timedelta(1)))
    ).order_by(MediaHistoryRecord.likes_count.desc()).limit(count)

    return records


def get_active_commenters(account, last_posts_count=10, popular_commenters_count=20):
    last_medias_ids = [m.id for m in session.query(Media).filter_by(
        owner_id=account.id).order_by(Media.created_time.desc()).limit(last_posts_count)]

    commenters = session.query(Comment.owner_id, func.count(Comment.id).label('total')).filter(
        Comment.media_id.in_(last_medias_ids)).group_by(Comment.owner_id).order_by(
        'total DESC').limit(popular_commenters_count)
    return commenters


if __name__ == '__main__':
    import random
    logging.basicConfig(level=logging.INFO)
    with open('popular_ig_accounts.json', 'r') as f:
        accounts_json = f.read()
        accounts_list = json.loads(accounts_json)
        random.shuffle(accounts_list, random.random)
    # import ipdb; ipdb.set_trace()
    collect_data(accounts_list)
    # ig = InstagramAPI(LOGIN, PASSWORD)
    # ig.login()
    # accounts = session.query(Account).filter(Account.followed_by_count > 0)
    # for acc in accounts:
    #     medias = get_account_feed(ig, acc)
    #     for media in medias:
    #         get_media_comments(ig, media)
