import requests
import math
import logging
import os
import datetime
from multiprocessing import Process, Queue
from time import sleep

from sqlalchemy import Column, ForeignKey, Integer, Float, String, Text, \
    DateTime, Date, Boolean, JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, configure_mappers, backref
from sqlalchemy import create_engine, func
from sqlalchemy_searchable import make_searchable
from sqlalchemy_utils.types import TSVectorType

VK_SECRET_KEY = 'hDBNyK5MIWimPao5z04D'
VK_ACCESS_KEY = 'ec3b6873ec3b6873ec3b6873b1ec67bd28eec3bec3b6873b57138206ddfc17ed077b0c0'
VERSION = '5.65'
REQUEST_URL = 'https://api.vk.com/method/{}'
DEFAULT_PARAMS = {'v': VERSION, 'access_token': VK_ACCESS_KEY}
MAX_COMMENTS_PER_REQUEST = 100
MAX_LIKES_PER_REQUEST = 1000
MAX_GROUPS_PER_REQUEST = 500
MAX_WALL_POSTS_PER_REQUEST = 100
WORKERS = []
MONITORING_PERIOD = 10

DB_URI = os.environ.get('DB_URI', 'postgres://smminer:smminer@127.0.0.1:5432/smminer')
Base = declarative_base()
make_searchable(options={'regconfig': 'pg_catalog.russian'})


def get_or_create(session, model, **kwargs):
    # Need to commit manually
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        instance = model(**kwargs)
        session.add(instance)
        return instance, True


class VKAPIError(Exception):
    pass


class Group(Base):
    __tablename__ = 'vk_groups'

    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    screen_name = Column(String(250), nullable=False)

    description = Column(Text)
    members_count = Column(Integer)
    photo_50 = Column(String(500))
    photo_100 = Column(String(500))
    photo_200 = Column(String(500))
    type = Column(String(100))

    members = relationship('User', secondary='vk_group_members')
    posts = relationship('Post', back_populates='group')

    category_id = Column(Integer, ForeignKey('categories.id'))
    category = relationship('Category', back_populates='groups')

    @classmethod
    def create_from_dict(cls, group_info):
        group, created = get_or_create(
            session, cls,
            id=int(group_info['id']))

        group.name = group_info['name']
        group.screen_name = group_info['screen_name']
        group.description = group_info.get('description')
        group.members_count = group_info.get('members_count')
        group.photo_50 = group_info.get('photo_50')
        group.photo_100 = group_info.get('photo_100')
        group.photo_200 = group_info.get('photo_200')
        group.type = group_info.get('type')
        session.add(group)
        session.commit()
        return group

    def __str__(self):
        return f'Group "{self.name}" (ID: {self.id})'


class Post(Base):
    __tablename__ = 'vk_posts'

    pk = Column(String(100), primary_key=True)
    post_id = Column(Integer, nullable=False)
    from_id = Column(Integer, nullable=False)
    owner_id = Column(Integer, nullable=False)
    date = Column(DateTime, nullable=False)
    group_id = Column(Integer, ForeignKey('vk_groups.id'))
    group = relationship('Group', back_populates='posts')

    is_pinned = Column(Boolean, default=False)
    marked_as_ads = Column(Boolean, default=False)
    type = Column(String(50))
    text = Column(Text)
    post_source = Column(String(100))

    views_count = Column(Integer)
    likes_count = Column(Integer)
    reposts_count = Column(Integer)
    comments_count = Column(Integer)

    liked_by_ids = Column(JSON)
    reposted_by_ids = Column(JSON)
    attachments = Column(JSON)

    history = relationship('PostHistoryRecord', back_populates='post')
    comments = relationship('Comment', back_populates='post')

    text_vector = Column(TSVectorType('text', regconfig='pg_catalog.russian'))

    def count_stats(self):
        q = self.history
        if len(q) < 2:
            return {}
        first_record, last_record = q[0], q[-1]

        views_diff = (last_record.views_count or 0) - (first_record.views_count or 0)
        likes_diff = (last_record.likes_count or 0) - (first_record.likes_count or 0)
        reposts_diff = (last_record.reposts_count or 0) - (first_record.reposts_count or 0)
        time_diff = (last_record.timestamp - first_record.timestamp).total_seconds()

        views_speed = round(views_diff / time_diff * 60, 5)
        likes_speed = round(likes_diff / time_diff * 60, 5)
        reposts_speed = round(reposts_diff / time_diff, 5)
        return {'period': time_diff, 'begin': first_record.timestamp, 'end': last_record.timestamp,
                'views_diff': views_diff, 'views_speed': views_speed,
                'likes_diff': likes_diff, 'likes_speed': likes_speed,
                'reposts_diff': reposts_diff, 'reposts_speed': reposts_speed,
                }

    def __str__(self):
        return f'Post {self.pk} by {self.from_id}'

    @classmethod
    def create_from_dict(cls, post_info, group):
        post, created = get_or_create(
            session, cls,
            pk=f'{post_info["owner_id"]}_{post_info["id"]}',  # '-' for group walls
            group=group
        )
        post.post_id = post_info['id']
        post.from_id = post_info['from_id']
        post.owner_id = post_info['owner_id']
        post.date = datetime.datetime.fromtimestamp(post_info['date'])
        post.is_pinned = bool(post_info.get('is_pinned', False))
        post.marked_as_ads = bool(post_info.get('marked_as_ads'))
        post.type = post_info.get('post_type')
        post.text = post_info.get('text')
        post.post_source = post_info.get('post_source', {}).get('type')
        post.views_count = post_info.get('views', {}).get('count')
        post.likes_count = post_info.get('likes', {}).get('count')
        post.reposts_count = post_info.get('reposts', {}).get('count')
        post.comments_count = post_info.get('comments', {}).get('count')
        post.attachments = post_info.get('attachments')
        session.add(post)
        session.commit()
        return post


class Comment(Base):
    __tablename__ = 'vk_comments'

    id = Column(Integer, primary_key=True)
    post_id = Column(String(100), ForeignKey('vk_posts.pk'), primary_key=True)
    post = relationship('Post', back_populates='comments')

    from_id = Column(Integer, nullable=False)  # TODO: Foreign key to User/Club
    date = Column(DateTime)
    text = Column(Text)
    likes_count = Column(Integer, nullable=False)
    attachments = Column(JSON)

    text_vector = Column(TSVectorType('text', regconfig='pg_catalog.russian'))

    def __str__(self):
        return f'Comment ID#{self.id} from {self.from_id} to post {self.post.pk}'


class PostHistoryRecord(Base):
    __tablename__ = 'vk_post_history_records'

    id = Column(Integer, primary_key=True)
    post_id = Column(String(100), ForeignKey('vk_posts.pk'))
    post = relationship('Post', back_populates='history')

    timestamp = Column(DateTime, default=datetime.datetime.now())
    views_count = Column(Integer)
    likes_count = Column(Integer)
    reposts_count = Column(Integer)


class GroupMembers(Base):
    __tablename__ = 'vk_group_members'
    group_id = Column(Integer, ForeignKey('vk_groups.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('vk_users.id'), primary_key=True)
    group = relationship('Group', backref=backref('group_members', cascade="all, delete-orphan"))
    user = relationship('User', backref=backref('group_members', cascade="all, delete-orphan"))


class User(Base):
    __tablename__ = 'vk_users'

    id = Column(Integer, primary_key=True)
    # data = Column(JSON)
    subscriptions = relationship('Group', secondary='vk_group_members')
    first_name = Column(String(200))
    last_name = Column(String(200))
    sex = Column(Boolean)
    birth_date = Column(Date)
    country = Column(Text)
    city = Column(Text)
    home_town = Column(Text)
    about = Column(Text)
    life_main = Column(Integer)
    people_main = Column(Integer)
    political = Column(Integer)
    religion = Column(Text)
    inspired_by = Column(Text)
    smoking = Column(Integer)
    alcohol = Column(Integer)
    languages = Column(Text)
    relation = Column(Integer)
    activities = Column(Text)
    interests = Column(Text)
    books = Column(Text)
    games = Column(Text)
    movies = Column(Text)
    music = Column(Text)
    tv = Column(Text)
    quotes = Column(Text)
    specialities = Column(Text)
    occupation = Column(Text)
    career = Column(Text)
    education = Column(Integer)
    followers_count = Column(Integer)
    has_photo = Column(Boolean)


class AdvertCampaign(Base):
    __tablename__ = 'advert_campaigns'
    id = Column(Integer, primary_key=True)
    age_min = Column(Integer)
    age_max = Column(Integer)
    male_min = Column(Float)
    female_min = Column(Float)
    cities = Column(Text)

    sub_count = Column(Integer)
    reach = Column(Integer)
    activity = Column(Float)

    category_id = Column(Integer, ForeignKey('categories.id'))
    category = relationship('Category', back_populates='adverts')
    keywords = Column(Text)
    description = Column(Text)


class Category(Base):
    __tablename__ = 'categories'
    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)

    groups = relationship('Group', back_populates='category')
    adverts = relationship('AdvertCampaign', back_populates='category')


class GroupStatsRecord(Base):
    __tablename__ = 'vk_group_stats'
    id = Column(Integer, primary_key=True)


# DB_URI = 'postgres://smminer:smminer@127.0.0.1:5432/smminer'

engine = create_engine(DB_URI)
configure_mappers()
Base.metadata.create_all(engine)

DBSession = sessionmaker(bind=engine)
session = DBSession()

###################################################


def get_groups_data(groups_list):
    # https://vk.com/dev/groups.getById
    group_fields = ['city', 'country', 'description', 'members_count']
    params = DEFAULT_PARAMS.copy()
    params.update({
        'group_ids': ','.join(groups_list),
        'fields': ','.join(group_fields),
    })
    response = requests.post(REQUEST_URL.format('groups.getById'), data=params)
    groups_data = response.json()['response']
    return groups_data


def get_groups(groups_list):
    logging.info('Getting groups')
    if len(groups_list) > MAX_GROUPS_PER_REQUEST:
        raise Exception(f'Too many groups: {MAX_GROUPS_PER_REQUEST} max (got {len(groups_list)}).')
    groups_data = get_groups_data(groups_list)
    groups = []
    for group_info in groups_data:
        try:
            group = Group.create_from_dict(group_info)
            groups.append(group)
        except KeyError:
            raise VKAPIError('Not enough information for group')
    logging.info('Finished getting groups')
    return groups


def get_wall_posts(group, count=MAX_WALL_POSTS_PER_REQUEST):
    # https://vk.com/dev/wall.get
    logging.info('Getting wall posts of group ' + group.name)
    group_id = '-' + str(group.id)

    posts_data, total = get_wall_posts_data(group_id, count)

    posts = []
    for post_info in posts_data:
        try:
            # post = Post.create_from_dict(post_info, group)
            post, created = get_or_create(
                session, Post,
                pk=f'{post_info["owner_id"]}_{post_info["id"]}',  # '-' for group walls
                group=group
            )
            post.post_id = post_info['id']
            post.from_id = post_info['from_id']
            post.owner_id = post_info['owner_id']
            post.date = datetime.datetime.fromtimestamp(post_info['date'])
            post.is_pinned = bool(post_info.get('is_pinned', False))
            post.marked_as_ads = bool(post_info.get('marked_as_ads'))
            post.type = post_info.get('post_type')
            post.text = post_info.get('text')
            post.post_source = post_info.get('post_source', {}).get('type')
            post.views_count = post_info.get('views', {}).get('count')
            post.likes_count = post_info.get('likes', {}).get('count')
            post.reposts_count = post_info.get('reposts', {}).get('count')
            post.comments_count = post_info.get('comments', {}).get('count')
            post.attachments = post_info.get('attachments')
            session.add(post)
            posts.append(post)
        except KeyError:
            session.rollback()
            raise VKAPIError('Not enough information about post')
    session.commit()
    return posts, total


def get_wall_posts_data(group_id, count=MAX_WALL_POSTS_PER_REQUEST):
    page_count = count if count <= MAX_WALL_POSTS_PER_REQUEST else MAX_WALL_POSTS_PER_REQUEST
    params = DEFAULT_PARAMS.copy()
    params.update({
        'owner_id': group_id,
        'count': page_count,
        'filter': 'owner',
        'extended': 1,
    })
    posts = []
    for page_num in range(math.ceil(count / MAX_WALL_POSTS_PER_REQUEST)):
        params['offset'] = page_num * MAX_WALL_POSTS_PER_REQUEST
        response = requests.get(REQUEST_URL.format('wall.get'), params=params)
        response_dict = response.json()['response']
        posts.extend(response_dict['items'])
    # get total count from last response
    total = response_dict['count']
    logging.info(f'Finished getting posts of {group_id}')
    return posts, total


def get_post_likes_reposts_ids(post):
    # https://vk.com/dev/likes.getList
    logging.info(f'Getting likes and reposts of post {post.owner_id}_{post.pk}')
    likes_count = post.likes_count
    reposts_count = post.reposts_count
    # get likes
    params = DEFAULT_PARAMS.copy()
    params.update({
        'type': 'post',
        'owner_id': post.owner_id,
        'item_id': post.post_id,
        'count': MAX_LIKES_PER_REQUEST,
    })

    logging.info('Fetching likes')
    likes_ids = []
    for page_num in range(math.ceil(likes_count / MAX_LIKES_PER_REQUEST)):
        params['offset'] = page_num * MAX_LIKES_PER_REQUEST
        response = requests.get(REQUEST_URL.format('likes.getList'), params=params)
        response_dict = response.json()['response']
        likes_ids += response_dict['items']

    # get reposts
    logging.info('Fetching reposts')
    params['filter'] = 'copies'
    reposts_ids = []
    for page_num in range(math.ceil(reposts_count / MAX_LIKES_PER_REQUEST)):
        params['offset'] = page_num * MAX_LIKES_PER_REQUEST
        response = requests.get(REQUEST_URL.format('likes.getList'), params=params)
        response_dict = response.json()['response']
        reposts_ids += response_dict['items']

    return likes_ids, reposts_ids


def get_post_comments(post):
    is_more_available = True
    comments = []
    params = DEFAULT_PARAMS.copy()
    params.update({
        'type': 'post',
        'owner_id': post.owner_id,
        'post_id': post.post_id,
        'count': MAX_COMMENTS_PER_REQUEST,
        'need_likes': 1,
        'sort': 'desc',
        'preview_length': 0,
        'extended': 1,
    })
    while is_more_available:
        response = requests.get(REQUEST_URL.format('wall.getComments'), params=params)
        response_dict = response.json()['response']
        for comment_data in response_dict['items']:
            comment, created = get_or_create(
                session, Comment,
                id=comment_data['id'],
                from_id=comment_data['from_id'],
                post=post
            )
            comment.date = datetime.datetime.fromtimestamp(comment_data['date'])
            comment.text = comment_data.get('text')
            comment.likes_count = comment_data.get('likes', {}).get('count')
            comment.attachments = comment_data.get('attachments')
            comments.append(comment)
            session.add(comment)
        session.commit()
        if len(response_dict['items']) < MAX_COMMENTS_PER_REQUEST:
            is_more_available = False
        else:
            params['start_comment_id'] = comments[-1].id
    logging.info('Got comments')
    return comments


def collect_data(groups_list, get_likes_reposts_ids=True):
    start_time = datetime.datetime.now()
    logging.info('Started at ' + str(start_time))
    groups = get_groups(groups_list)

    for group in groups:
        posts, total = get_wall_posts(group, 1000)
        # try:
        #     posts, total = get_wall_posts(group, 30)
        # except:
        #     posts, total = [], 0
        #     session.rollback()
        logging.info(f'{group.name}: {total}')
        if get_likes_reposts_ids:
            for post in posts:
                liked_by, reposted_by = get_post_likes_reposts_ids(post)
                post.liked_by_ids = liked_by
                post.reposted_by_ids = reposted_by
                session.add(post)
            session.commit()

    logging.info('Ended at ' + str(datetime.datetime.now()))


def setup_groups_monitoring(groups_list, posts_count=20):
    CPU_COUNT = 1
    queue = Queue()
    for group in groups_list:
        queue.put(group)
    for _ in range(CPU_COUNT):
        queue.put(None)
    for _ in range(CPU_COUNT):
        p = Process(target=monitor_consumer, args=(queue, posts_count))
        p.start()
        WORKERS.append(p)


def monitor_consumer(queue, count):
    while True:
        group_name_or_id = queue.get()
        if group_name_or_id is None:
            break
        get_last_posts_stats(group_name_or_id, count)


def get_last_posts_stats(group_name_or_id, count):
    # group = Group.get((Group.name == group_name_or_id) | (Group.group_id == group_name_or_id))
    group = session.query(Group).filter(
        (Group.name == str(group_name_or_id)) | (Group.id == group_name_or_id)).first()
    if not group:
        logging.warning(f'No group found with ID or name {group_name_or_id}')
        return []
    group_id = f'-{group.id}'
    posts_data, total = get_wall_posts_data(group_id, count)
    post_ids = []
    for post_info in posts_data:
        post = session.query(Post).filter(
            (Post.post_id == post_info['id']) & (Post.from_id == post_info['from_id'])).first()
        if not post:
            post = Post.create_from_dict(post_info, group)
        post.is_pinned = bool(post_info.get('is_pinned', False))
        views_count = post_info.get('views', {}).get('count')
        likes_count = post_info.get('likes', {}).get('count')
        reposts_count = post_info.get('reposts', {}).get('count')
        if views_count or likes_count or reposts_count:
            record = PostHistoryRecord(post=post)
            record.views_count = views_count
            post.views_count = views_count or post.views_count
            record.likes_count = likes_count
            post.likes_count = likes_count or post.likes_count
            record.reposts_count = reposts_count
            post.reposts_count = reposts_count or post.reposts_count
            session.add(record)
        session.add(post)
        session.commit()
        post_ids.append(post.pk)
    return post_ids


def get_most_popular_posts_for_day(date, count=100):
    results = session.query(Post).join(PostHistoryRecord, Post.pk == PostHistoryRecord.post_id).filter(
        (PostHistoryRecord.likes_count > 0) &
        (PostHistoryRecord.timestamp >= date) &
        (PostHistoryRecord.timestamp < (date + datetime.timedelta(1)))
    ).order_by(PostHistoryRecord.likes_count.desc()).limit(count)

    return results


def get_popular_comments(group, last_posts_count=20, top_comments_count=20):
    last_posts = session.query(Post).filter_by(group_id=group.id).order_by(Post.date.desc()).limit(last_posts_count)
    last_posts_ids = [f'-{group.id}_{p.post_id}' for p in last_posts]
    top_comments = session.query(Comment).filter(Comment.post_id.in_(last_posts_ids)).order_by(Comment.likes_count.desc()).limit(top_comments_count)
    return top_comments


def get_active_commenters(group, last_posts_count=10, popular_commenters_count=20):
    last_posts = session.query(Post).filter_by(group_id=group.id).order_by(Post.date.desc()).limit(last_posts_count)
    last_posts_ids = [f'-{group.id}_{p.post_id}' for p in last_posts]
    commenters = session.query(Comment.from_id, func.count(Comment.id).label('total')).filter(
        Comment.post_id.in_(last_posts_ids)).group_by(
            Comment.from_id).order_by('total DESC').limit(
            popular_commenters_count)
    return commenters


def guess_user_education_level(schools_list, uni_list):
    """
    Education:
    0 - нет данных
    1 - учащийся
    2 - основное/среднее общее
    3 - среднее профессиональное
    4 - высшее (бакалавр, магистр, специалист)
    """
    if not schools_list and not uni_list:
        return 0
    today = datetime.date.today()
    next_year = today.year + 1
    for uni in uni_list:
        if uni.get('graduation', next_year) <= today.year:
            return 4
    schools_list.reverse()
    for school in schools_list:
        has_graduated = school.get('year_graduated', next_year) <= today.year
        school_type = school.get('type', 0)
        # TODO: is_studying
        if has_graduated:
            if school_type > 7:  # 8..13
                return 3
            elif school_type < 4:  # 0..3
                return 2
    return 0


def create_user(user_data, is_update_needed=False):
    # Filter data from API. Keep only relevant information.
    # drop deactivated
    user = None
    is_user_created = False
    if user_data.get('deactivated'):
        return user
    user = session.query(User).get(user_data['id'])
    if not user:
        user = User(id=user_data['id'])
        is_user_created = True
    if is_user_created or is_update_needed:
        # Biological
        user.first_name = user_data['first_name']
        user.last_name = user_data['last_name']
        # Sex: 0 - NULL, 1 - female, 2 - male -> NULL, False, True
        user.sex = None if user_data['sex'] == 0 else bool(user_data['sex'] - 1)

        birth_date_str = user_data.get('bdate', '')
        if birth_date_str.count('.') == 2:
            user.birth_date = datetime.datetime.strptime(
                user_data['bdate'], '%d.%m.%Y').date()
        # user.relatives = list [[id, name, type]]

        # Geographical
        user.country = user_data.get('country', {}).get('title')
        user.city = user_data.get('city', {}).get('title')
        user.home_town = user_data.get('home_town', '') or None

        # Personal
        user.about = user_data.get('about')
        user.relation = user_data.get('relation')
        personal = user_data.get('personal', {})
        user.languages = ''.join(personal.get('langs', [])) or None
        user.political = personal.get('political')
        user.religion = personal.get('religion')
        user.inspired_by = personal.get('inspired_by')
        user.life_main = personal.get('life_main')
        user.people_main = personal.get('people_main')
        user.smoking = personal.get('smoking')  # 1..5
        user.alcohol = personal.get('alcohol')  # 1..5
        # user.relation_partner = user_data.get('relation_partner', {}).get('id')  # Another user

        # Interests
        user.activities = user_data.get('activities')
        user.interests = user_data.get('interests')
        user.books = user_data.get('books')
        user.games = user_data.get('games')
        user.movies = user_data.get('movies')
        user.music = user_data.get('music')
        user.tv = user_data.get('tv')
        user.quotes = user_data.get('quotes')

        # Career/education
        schools_list = user_data.get('schools', [{}])
        uni_list = user_data.get('universities', [{}])
        specialities = [school.get('speciality', '') for school in schools_list]
        specialities.extend([s for uni in uni_list
                             for s in (uni.get('faculty_name', ''), uni.get('chair_name', ''))])
        user.specialities = ';'.join(specialities)
        user.occupation = user_data.get('occupation', {}).get('type')
        career_list = user_data.get('career') or [{}]
        user.career = career_list[-1].get('position')
        user.education = guess_user_education_level(schools_list, uni_list)

        # user.schools = list [[id, type, year, ...]]
        # user.universities = list [[id, type, year, ...]]

        user.followers_count = user_data.get('followers_count')
        user.has_photo = bool(user_data.get('has_photo', 0))
        # user.data = profile
    return user


def get_group_members(group):
    logging.info(f'Getting members of group {group}')
    MAX_PROFILES_COUNT = 1000
    params = DEFAULT_PARAMS.copy()
    params.update({
        'group_id': group.id,
        'sort': 'id_asc',
        'count': MAX_PROFILES_COUNT,
        'fields': ','.join(['verified', 'sex', 'bdate', 'city', 'country',
                            'home_town', 'has_photo', 'education', 'universities',
                            'schools', 'last_seen', 'followers_count',
                            'occupation', 'relatives', 'relation',
                            'personal', 'connections', 'activities',
                            'interests', 'music', 'movies', 'tv', 'books',
                            'games', 'about', 'quotes', 'maiden_name', 'career']),
    })
    members_count = get_groups_data([str(group.id)])[0].get('members_count', group.members_count)
    is_more_available = True
    offset = 0
    # for offset in range(0, members_count, MAX_PROFILES_COUNT):
    while is_more_available:
        logging.info(f'Offset: {offset}/{members_count}')
        params['offset'] = offset
        response = requests.post(REQUEST_URL.format('groups.getMembers'), data=params)
        users_info = response.json()['response']
        members_count = users_info['count']
        users_info = users_info['items']
        logging.info('Fetched users')
        for data in users_info:
            user = create_user(data, True)
            if user:
                user.subscriptions.append(group)
                session.add(user)
        session.commit()
        offset += MAX_PROFILES_COUNT
        is_more_available = offset < members_count and len(users_info) > 0


def get_audience_characteristics(group):
    audience = {}
    users = session.query(User).filter(User.id.in_(
        session.query(GroupMembers.user_id).filter_by(group_id=group.id)))
    # 0. Total users
    total_users = users.count()
    if not total_users:
        return {}
    audience['total_users'] = total_users

    # 1. Male-female
    males = users.filter_by(sex=True).count()
    females = users.filter_by(sex=False).count()
    males_percentage = males / total_users
    females_percentage = females / total_users
    # print(f'Males: {males_percentage}; Females: {females_percentage}\n')
    audience['sex'] = {'male': males_percentage, 'female': females_percentage}

    # 2. Geoposition. Top 20 cities. Top 5 countries.
    top_cities = session.query(User.city, func.count(User.id).label('city_total')).filter(User.id.in_(
        session.query(GroupMembers.user_id).filter_by(group_id=group.id))).group_by(
        User.city).order_by('city_total DESC').limit(20).all()
    # top_cities_str = '\n'.join([f'{index+1}. {city[0]}: {city[1] / total_users}' for index, city in enumerate(top_cities)])
    # print(f'Top cities:\n{top_cities_str}\n')

    top_countries = session.query(User.country, func.count(User.id).label('country_total')).filter(User.id.in_(
        session.query(GroupMembers.user_id).filter_by(group_id=group.id))).group_by(
        User.country).order_by('country_total DESC').limit(5).all()
    # top_countries_str = '\n'.join([f'{index+1}. {country[0]}: {country[1] / total_users}' for index, country in enumerate(top_countries)])
    # print(f'Top countries:\n{top_countries_str}\n')
    audience['geo'] = {
        'city': [(city[0], city[1] / total_users) for city in top_cities],
        'country': [(country[0], country[1] / total_users) for country in top_countries]
    }

    # 3. Age groups
    today = datetime.date.today()
    AGE_CATEGORIES = [
        {'min': 0, 'max': 14},
        {'min': 14, 'max': 18},
        {'min': 18, 'max': 21},
        {'min': 21, 'max': 24},
        {'min': 24, 'max': 27},
        {'min': 27, 'max': 30},
        {'min': 30, 'max': 35},
        {'min': 35, 'max': 45},
        {'min': 45, 'max': 120}
    ]
    ages = []
    ages_sex = {'male': [], 'female': []}
    total_users_with_bdate = users.filter(User.birth_date > datetime.date(1, 1, 1)).count()
    for category in AGE_CATEGORIES:
        min_date = datetime.date(year=today.year - category['max'], month=today.month, day=today.day)
        max_date = datetime.date(year=today.year - category['min'], month=today.month, day=today.day)
        users_in_age_cat = users.filter((User.birth_date > min_date) & (User.birth_date < max_date))
        users_in_age_cat_count = users_in_age_cat.count()

        males_in_age_cat_count = users_in_age_cat.filter_by(sex=True).count()
        females_in_age_cat_count = users_in_age_cat.filter_by(sex=False).count()
        ages.append(users_in_age_cat_count / total_users_with_bdate)
        ages_sex['male'].append(males_in_age_cat_count / total_users_with_bdate)
        ages_sex['female'].append(females_in_age_cat_count / total_users_with_bdate)
    # ages_str = '\n'.join([f"{AGE_CATEGORIES[i]['min']}-{AGE_CATEGORIES[i]['max']}: "
    #                       f"{age_percent} (M: {ages_sex['male'][i]}, F: {ages_sex['female'][i]})" for i, age_percent in enumerate(ages)])
    # print(f'Age categories:\n{ages_str}\n')
    audience['age'] = [{'min': category['min'], 'max': category['max'],
                        'm_percent': ages_sex['male'][i], 'f_percent': ages_sex['female'][i],
                        'total_percent': ages[i]} for i, category in enumerate(AGE_CATEGORIES)]

    # 4. Try to find group content category
    ext_url = 'http://allsocial.ru/entity'
    CONTENT_CATEGORIES_NAMES = [None, 'авто/мото', 'активный отдых', 'бизнес', 'домашние животные',
                                'здоровье', 'знакомство и общение', 'игры', 'ИТ (компьютеры и софт)',
                                'кино', 'красота и мода', 'кулинария', 'культура и искусство', 'литература',
                                'мобильная связь и интернет', 'музыка', 'наука и техника', 'недвижимость',
                                'новости и СМИ', 'безопасность', 'образование', 'обустройство и ремонт',
                                'политика', 'продукты питания', 'промышленность', 'путешествия', 'работа',
                                'развлечения', 'религия', 'дом и семья', 'спорт', 'страхование', 'телевидение',
                                'товары и услуги', 'увлечения и хобби', 'финансы', 'фото', 'эзотерика',
                                'электроника и бытовая техника', 'эротика', 'юмор', 'общество, гуманитарные науки',
                                'дизайн и графика']
    group_categories = []
    try:
        response = requests.get(ext_url, {'str': group.name}).json()
        group_categories = next((g for g in response['response']['entity'] if g['vk_id'] == group.id))
        group_categories = [int(x) for x in (set(group_categories['category']['public']) | set(group_categories['category']['private']))]
        # content_categories_str = ', '.join([CONTENT_CATEGORIES_NAMES[i] for i in group_categories])
        # print(f'Categories: {content_categories_str}\n')

        audience['content'] = [CONTENT_CATEGORIES_NAMES[i] for i in group_categories if i < len(CONTENT_CATEGORIES_NAMES)]
    except:
        logging.warning('Failed to get group content category')
    return audience


def find_ads_for_group(group, stats):
    query = session.query(AdvertCampaign)
    # 1. Filter by category
    if group.category:
        query = query.filter_by(category=group.category)
    # 2. Filter by sex percentage
    query.filter(AdvertCampaign.male_min <= stats['sex']['male'])
    query.filter(AdvertCampaign.female_min <= stats['sex']['female'])
    # 3. Filter by top city
    top_cities = stats['geo']['city']
    query.filter(AdvertCampaign.cities.in_(top_cities))
    # 4. Filter by age groups
    ages = [age for g in stats['age'] for age in range(g['min'], g['max'] + 1)]
    query.filter((AdvertCampaign.age_min.in_(ages)) | (AdvertCampaign.age_max.in_(ages)))
    # 5. Filter by sub_count
    query.filter(AdvertCampaign.sub_count <= group.members_count)
    return query.limit(20)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    # import json
    # with open('popular_vk_groups.json', 'r') as f:
    #     groups_json = f.read()
    #     groups_list = json.loads(groups_json)
    # collect_data(groups_list, False)
    sample_group_id = 36965584
    collect_data([str(sample_group_id)], False)
    group = session.query(Group).get(sample_group_id)
    # get_group_members(group)
    posts = group.posts[:1000]
    for post in posts:
        if not post.is_pinned:
            get_post_comments(post)
    # setup_groups_monitoring(groups_list)
    # for _ in range(20):
    #     post_ids = get_last_posts_stats('76982440', 30)
    #     for post_id in post_ids:
    #         post = session.query(Post).filter_by(pk=post_id).first()
    #         if not post.is_pinned:
    #             get_post_comments(post)
    #         logging.info(f'Got post {post_id} with comments')
    #     sleep(60)
