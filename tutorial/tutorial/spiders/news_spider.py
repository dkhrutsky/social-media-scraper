import scrapy


VK_SECRET_KEY = 'o6WiEhofUGnhQSyJIj6E'
VK_ACCESS_KEY = '2bf2de4e2bf2de4e2bf2de4e2f2bae0b1522bf22bf2de4e72b88e2f80d6ea5b213b03a9'


class NewsSpider(scrapy.Spider):
    name = 'news'
    start_urls = [
        'https://vk.com/tj',
    ]

    def parse(self, response):
        vk_post_css = 'div._post.post'
        likes_count_css = 'span.post_like_count::text'
        post_text_css = 'div.wall_post_text'
        for post in response.css(vk_post_css):
            self.log(str({
                'likes': post.css(likes_count_css).extract_first(),
                'text': post.css(post_text_css).extract_first(),
            }))


class VKAPISpider(scrapy.Spider):
    name = 'vk_api'
    request_url = 'https://api.vk.com/method/{method_name}?{parameters}'
                  '&access_token={access_token}&v=5.65'
    methods = {
        'group_meta': {'method_name': '', 'parameters': ''},
        'group_wall': {'method_name': '', 'parameters': ''},
        'post_info': {'method_name': '', 'parameters': ''},
    }

    def __init__(self, page_names=[], *args, **kwargs):
        self.page_names = page_names
        super(VKAPISpider, self).__init__(*args, **kwargs)
